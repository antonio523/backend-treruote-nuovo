package daointerface;

import java.util.List;

import dao.GeneralDao;
import model.Categoria;

public interface CategoriaDaoInterface extends GeneralDao{

	public boolean aggiungiCategoria(Categoria c);
	public void rimuoviCategoria(Categoria c);
	public List<Categoria> getCategoria();
	public Categoria getSingleCategoriaByNome(String stringa);
}