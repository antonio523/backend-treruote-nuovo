package daointerface;

import java.util.List;

import dao.GeneralDao;
import model.Composizione;

public interface ComposizioneDaoInterface extends GeneralDao {

	public boolean aggiungiComposizione(Composizione c);
	public void rimuoviComposizione(Composizione c);
	public List<Composizione> getComposizione();
}
