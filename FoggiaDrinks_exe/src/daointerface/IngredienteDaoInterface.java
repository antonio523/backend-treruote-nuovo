package daointerface;

import java.util.List;

import dao.GeneralDao;
import model.Ingrediente;

public interface IngredienteDaoInterface extends GeneralDao {

	public boolean aggiungiIngrediente(Ingrediente i);
	public void rimuoviIngrediente(Ingrediente i);
	public List<Ingrediente> getIngrediente();
	public Ingrediente getSingleIngredienteByNome(String stringa);
}
