package daointerface;

import java.util.List;

import dao.GeneralDao;
import model.Drink;

public interface DrinkDaoInterface extends GeneralDao{

	public boolean aggiungiDrink(Drink c);
	public void rimuoviDrink(Drink c);
	public List<Drink> getDrink();
	public Drink getSingleDrinkByNome(String stringa);
	public boolean updateDrink(Drink d);
}
