package dao;

import daointerface.CategoriaDaoInterface;
import daointerface.DrinkDaoInterface;
import daointerface.IngredienteDaoInterface;
import daointerface.UtenteDaoInterface;
import daointerface.SelezioneDaoInterface;
import daointerface.ComposizioneDaoInterface;

public abstract class DaoFactory {

	public abstract CategoriaDaoInterface CategoriaDaoInterface();

	public abstract UtenteDaoInterface UtenteDaoInterface();

	public abstract SelezioneDaoInterface SelezioneDaoInterface();

	public abstract DrinkDaoInterface DrinkDaoInterface();

	public abstract ComposizioneDaoInterface ComposizioneDaoInterface();

	public abstract IngredienteDaoInterface IngredienteDaoInterface();

	public static DaoFactory getDaoFactory() {

		return new JPADaoFactory();
	}
}