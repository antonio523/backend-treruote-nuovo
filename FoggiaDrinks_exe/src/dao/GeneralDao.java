package dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public interface GeneralDao {

	public default EntityManager getManager() {
		
		EntityManager manager = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			EntityManagerFactory factory = Persistence.createEntityManagerFactory("FoggiaDrinksexe");
			manager = factory.createEntityManager();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return manager;
	}

}