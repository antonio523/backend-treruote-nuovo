package dao;

import daoclass.JPACategoriaDao;
import daoclass.JPAComposizioneDao;
import daoclass.JPADrinkDao;
import daoclass.JPAIngredienteDao;
import daoclass.JPASelezioneDao;
import daoclass.JPAUtenteDao;
import daointerface.ComposizioneDaoInterface;
import daointerface.DrinkDaoInterface;
import daointerface.IngredienteDaoInterface;
import daointerface.SelezioneDaoInterface;
import daointerface.UtenteDaoInterface;
import daointerface.CategoriaDaoInterface;

public class JPADaoFactory extends DaoFactory {
	
	@Override
	public CategoriaDaoInterface CategoriaDaoInterface() {
		return JPACategoriaDao.getInstance();	
	}

	@Override
	public UtenteDaoInterface UtenteDaoInterface() {
		return JPAUtenteDao.getInstance();
	}

	@Override
	public SelezioneDaoInterface SelezioneDaoInterface() {
		return JPASelezioneDao.getInstance();
	}

	@Override
	public DrinkDaoInterface DrinkDaoInterface() {
		return JPADrinkDao.getInstance();
	}

	@Override
	public ComposizioneDaoInterface ComposizioneDaoInterface() {
		return JPAComposizioneDao.getInstance();
	}

	@Override
	public IngredienteDaoInterface IngredienteDaoInterface() {
		return JPAIngredienteDao.getInstance();
	}



}
