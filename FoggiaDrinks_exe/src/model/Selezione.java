package model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;

@Entity
@NamedQuery(name="Selezione.findAll", query="SELECT s FROM Selezione s")
public class Selezione implements Serializable { 

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id_selezione;
	
	@ManyToOne(fetch=FetchType.LAZY)
	//@MapsId("id")
	@JoinColumn(name="id_drink_sel",referencedColumnName="id_drink")
	private Drink drink;
	
	@ManyToOne(fetch=FetchType.LAZY)
	//@MapsId("id")
	@JoinColumn(name="id_utente_sel",referencedColumnName="id_utente")
	private Utente utente;
	
	private float feedback;

	public Selezione(int id_selezione, Drink drink, Utente utente, float feedback) {
		super();
		this.id_selezione = id_selezione;
		this.drink = drink;
		this.utente = utente;
		this.feedback = feedback;
	}

	public Selezione(Drink drink, Utente utente, float feedback) {
		super();
		this.drink = drink;
		this.utente = utente;
		this.feedback = feedback;
	}

	public Selezione() {
		super();
	}

	public int getId_selezione() {
		return id_selezione;
	}

	public void setId_selezione(int id_selezione) {
		this.id_selezione = id_selezione;
	}

	public Drink getDrink() {
		return drink;
	}

	public void setDrink(Drink drink) {
		this.drink = drink;
	}

	public Utente getUtente() {
		return utente;
	}

	public void setUtente(Utente utente) {
		this.utente = utente;
	}

	public float getFeedback() {
		return feedback;
	}

	public void setFeedback(float feedback) {
		this.feedback = feedback;
	}

	@Override
	public String toString() {
		return "Selezione [id_selezione=" + id_selezione + ", drink=" + drink + ", utente=" + utente + ", feedback="
				+ feedback + "]";
	}
	

}
