package model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;

	@Entity
	@NamedQuery(name="Composizione.findAll", query="SELECT c FROM Composizione c")
	public class Composizione implements Serializable { 

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Id
		@GeneratedValue(strategy=GenerationType.IDENTITY)
		@Column(name="id_composizione")
		private int id_composizione;
		
		@ManyToOne(fetch=FetchType.LAZY)
		@JoinColumn(name="id_drink_comp",referencedColumnName="id_drink")
		private Drink drink;
		
		@ManyToOne(fetch=FetchType.LAZY)
		@JoinColumn(name="id_ingrediente_comp",referencedColumnName="id_ingrediente")
		private Ingrediente ingrediente;
		
		@Column(name="misura")
		private String misura;

		public Composizione(int id_composizione, Drink drink, Ingrediente ingrediente, String misura) {
			super();
			this.id_composizione = id_composizione;
			this.drink = drink;
			this.ingrediente = ingrediente;
			this.misura = misura;
		}

		public Composizione(Drink drink, Ingrediente ingrediente, String misura) {
			super();
			this.drink = drink;
			this.ingrediente = ingrediente;
			this.misura = misura;
		}

		public Composizione() {
			super();
		}

		public int getId_composizione() {
			return id_composizione;
		}

		public void setId_composizione(int id_composizione) {
			this.id_composizione = id_composizione;
		}

		public Drink getDrink() {
			return drink;
		}

		public void setDrink(Drink drink) {
			this.drink = drink;
		}

		public Ingrediente getIngrediente() {
			return ingrediente;
		}

		public void setIngrediente(Ingrediente ingrediente) {
			this.ingrediente = ingrediente;
		}

		public String getMisura() {
			return misura;
		}

		public void setMisura(String misura) {
			this.misura = misura;
		}

		@Override
		public String toString() {
			return "Composizione [id_composizione=" + id_composizione + ", drink=" + drink + ", ingrediente="
					+ ingrediente + ", misura=" + misura + "]";
		}
		
		
		
		


}
