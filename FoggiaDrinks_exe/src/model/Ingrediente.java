package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the ingrediente database table.
 * 
 */
@Entity
@NamedQuery(name="Ingrediente.findAll", query="SELECT i FROM Ingrediente i")
public class Ingrediente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_ingrediente")
	private int idIngrediente;

	@Column(name="nome")
	private String nome;

	public Ingrediente() {
	}	

	public Ingrediente(String nome) {
		super();
		this.nome = nome;
	}

	public Ingrediente(int idIngrediente, String nome) {
		super();
		this.idIngrediente = idIngrediente;
		this.nome = nome;
	}



	public int getIdIngrediente() {
		return this.idIngrediente;
	}

	public void setIdIngrediente(int idIngrediente) {
		this.idIngrediente = idIngrediente;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
}
