package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the drink database table.
 * 
 */
@Entity
@NamedQuery(name="Drink.findAll", query="SELECT d FROM Drink d")
public class Drink implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_drink")
	private int idDrink;

	@Column(name="nome")
	private String nome;

	@Column(name="bicchiere")
	private String bicchiere;

	
	@Column(name="alcool_no_alcool")
	private String alcoolNoAlcool;
	
	//uni-directional many-to-one association to Categoria
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_categoria_dri", referencedColumnName = "id_categoria")
	private Categoria categoria;

	@Column(name="immagine")
	private String immagine;

	public Drink(DrinkJson drink, Categoria c) {
		super();
		this.idDrink = drink.getIdDrink();
		this.alcoolNoAlcool = drink.getStrAlcoholic();
		this.bicchiere = drink.getStrGlass();
		this.nome = drink.getStrDrink();
		this.immagine = drink.getStrDrinkThumb();
		this.categoria = c;
	}

	public Drink() {
	}

	public int getIdDrink() {
		return this.idDrink;
	}

	public void setIdDrink(int idDrink) {
		this.idDrink = idDrink;
	}

	public String getAlcoolNoAlcool() {
		return this.alcoolNoAlcool;
	}

	public void setAlcoolNoAlcool(String alcoolNoAlcool) {
		this.alcoolNoAlcool = alcoolNoAlcool;
	}

	public String getBicchiere() {
		return this.bicchiere;
	}

	public void setBicchiere(String bicchiere) {
		this.bicchiere = bicchiere;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Categoria getCategoria() {
		return this.categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public String getImmagine() {
		return immagine;
	}

	public void setImmagine(String immagine) {
		this.immagine = immagine;
	}
	
	


}