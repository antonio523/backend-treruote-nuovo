package datafound;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import dao.DaoFactory;
import daoclass.JPACategoriaDao;
import daoclass.JPAComposizioneDao;
import daoclass.JPADrinkDao;
import daoclass.JPAIngredienteDao;
import model.Categoria;
import model.Composizione;
import model.Drink;
import model.DrinkJson;
import model.Ingrediente;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class DataFound {

	public static void getDati(){
		List <Drink> mieiDrinks = new ArrayList<Drink>();
		List<DrinkJson> miaLista=null;
		
		char[] alfabeto = "'12345679abcdefghijklmnopqrstvwyz".toCharArray();
		for (int i = 0; i < alfabeto.length; i++) {
			String path="https://www.thecocktaildb.com/api/json/v1/1/search.php?f="+alfabeto[i];
			
			Request request =new Request.Builder()
					.url(path)
					.build();
			String requestResult=null;
			try {
				Response response=new OkHttpClient().newCall(request).execute();
				if(response.code()==200) {
					requestResult=response.body().string().replace("{\"drinks\":", "").replace("]}", "]");
					
					try {						
						TypeToken<List<DrinkJson>> t=new TypeToken<List<DrinkJson>>(){};
						miaLista=new Gson().fromJson(requestResult, t.getType());
						
					} catch (Exception e) {
						e.printStackTrace();
					}
					for (int j = 0; j < miaLista.size(); j++) {
						Categoria c =null;
						Drink drink=null;
						Ingrediente i1=null
								,i2=null
								,i3=null
								,i4=null
								,i5=null
								,i6=null
								,i7=null
								,i8=null
								,i9=null
								,i10=null
								,i11=null
								,i12=null
								,i13=null
								,i14=null
								,i15=null;
						/*try {
							c= new Categoria(miaLista.get(j));
							//dao push categoria
							DaoFactory.getDaoFactory().CategoriaDaoInterface().aggiungiCategoria(c);
							
						} catch (Exception e) {
							// TODO: handle exception
						}*/
						
						/*try {
							DrinkJson napoli=miaLista.get(j);
							c=DaoFactory.getDaoFactory().CategoriaDaoInterface().getSingleCategoriaByNome(miaLista.get(j).getStrCategory());
							 drink= new Drink(napoli,c);
							//dao push Drink
							DaoFactory.getDaoFactory().DrinkDaoInterface().aggiungiDrink(drink);
							
						} catch (Exception e) {
							// TODO: handle exception
						}*/
						
						try {
							
							drink=DaoFactory.getDaoFactory().DrinkDaoInterface().getSingleDrinkByNome(miaLista.get(j).getStrDrink());
							
							
							i1=DaoFactory.getDaoFactory().IngredienteDaoInterface().getSingleIngredienteByNome(miaLista.get(j).getStrIngredient1());
							i2=DaoFactory.getDaoFactory().IngredienteDaoInterface().getSingleIngredienteByNome(miaLista.get(j).getStrIngredient2());
							i3=DaoFactory.getDaoFactory().IngredienteDaoInterface().getSingleIngredienteByNome(miaLista.get(j).getStrIngredient3());
							i4=DaoFactory.getDaoFactory().IngredienteDaoInterface().getSingleIngredienteByNome(miaLista.get(j).getStrIngredient4());
							i5=DaoFactory.getDaoFactory().IngredienteDaoInterface().getSingleIngredienteByNome(miaLista.get(j).getStrIngredient5());
							i6=DaoFactory.getDaoFactory().IngredienteDaoInterface().getSingleIngredienteByNome(miaLista.get(j).getStrIngredient6());
							i7=DaoFactory.getDaoFactory().IngredienteDaoInterface().getSingleIngredienteByNome(miaLista.get(j).getStrIngredient7());
							i8=DaoFactory.getDaoFactory().IngredienteDaoInterface().getSingleIngredienteByNome(miaLista.get(j).getStrIngredient8());
							i9=DaoFactory.getDaoFactory().IngredienteDaoInterface().getSingleIngredienteByNome(miaLista.get(j).getStrIngredient9());
							i10=DaoFactory.getDaoFactory().IngredienteDaoInterface().getSingleIngredienteByNome(miaLista.get(j).getStrIngredient10());
							i11=DaoFactory.getDaoFactory().IngredienteDaoInterface().getSingleIngredienteByNome(miaLista.get(j).getStrIngredient11());
							i12=DaoFactory.getDaoFactory().IngredienteDaoInterface().getSingleIngredienteByNome(miaLista.get(j).getStrIngredient12());
							i13=DaoFactory.getDaoFactory().IngredienteDaoInterface().getSingleIngredienteByNome(miaLista.get(j).getStrIngredient13());
							i14=DaoFactory.getDaoFactory().IngredienteDaoInterface().getSingleIngredienteByNome(miaLista.get(j).getStrIngredient14());
							i15=DaoFactory.getDaoFactory().IngredienteDaoInterface().getSingleIngredienteByNome(miaLista.get(j).getStrIngredient15());
							
						/*	i1 = new Ingrediente(miaLista.get(j).getStrIngredient1());
							i2 = new Ingrediente(miaLista.get(j).getStrIngredient2());
							i3 = new Ingrediente(miaLista.get(j).getStrIngredient3());
							i4 = new Ingrediente(miaLista.get(j).getStrIngredient4());
							i5 = new Ingrediente(miaLista.get(j).getStrIngredient5());
							i6 = new Ingrediente(miaLista.get(j).getStrIngredient6());
							i7 = new Ingrediente(miaLista.get(j).getStrIngredient7());
							i8 = new Ingrediente(miaLista.get(j).getStrIngredient8());
							i9 = new Ingrediente(miaLista.get(j).getStrIngredient9());
							i10 = new Ingrediente(miaLista.get(j).getStrIngredient10());
							i11 = new Ingrediente(miaLista.get(j).getStrIngredient11());
							i12 = new Ingrediente(miaLista.get(j).getStrIngredient12());
							i13 = new Ingrediente(miaLista.get(j).getStrIngredient13());
							i14 = new Ingrediente(miaLista.get(j).getStrIngredient14());
							i15 = new Ingrediente(miaLista.get(j).getStrIngredient15());
							*///dao push ingredient
						/*	DaoFactory.getDaoFactory().IngredienteDaoInterface().aggiungiIngrediente(i1);
							DaoFactory.getDaoFactory().IngredienteDaoInterface().aggiungiIngrediente(i2);
							DaoFactory.getDaoFactory().IngredienteDaoInterface().aggiungiIngrediente(i3);
							DaoFactory.getDaoFactory().IngredienteDaoInterface().aggiungiIngrediente(i4);
							DaoFactory.getDaoFactory().IngredienteDaoInterface().aggiungiIngrediente(i5);
							DaoFactory.getDaoFactory().IngredienteDaoInterface().aggiungiIngrediente(i6);
							DaoFactory.getDaoFactory().IngredienteDaoInterface().aggiungiIngrediente(i7);
							DaoFactory.getDaoFactory().IngredienteDaoInterface().aggiungiIngrediente(i8);
							DaoFactory.getDaoFactory().IngredienteDaoInterface().aggiungiIngrediente(i9);
							DaoFactory.getDaoFactory().IngredienteDaoInterface().aggiungiIngrediente(i10);
							DaoFactory.getDaoFactory().IngredienteDaoInterface().aggiungiIngrediente(i11);
							DaoFactory.getDaoFactory().IngredienteDaoInterface().aggiungiIngrediente(i12);
							DaoFactory.getDaoFactory().IngredienteDaoInterface().aggiungiIngrediente(i13);
							DaoFactory.getDaoFactory().IngredienteDaoInterface().aggiungiIngrediente(i14);
							DaoFactory.getDaoFactory().IngredienteDaoInterface().aggiungiIngrediente(i15);
							*/
						} catch (Exception e) {
							
						}
						
						
						
						String m1 = miaLista.get(j).getStrMeasure1();
						String m2 = miaLista.get(j).getStrMeasure2();
						String m3 = miaLista.get(j).getStrMeasure3();
						String m4 = miaLista.get(j).getStrMeasure4();
						String m5 = miaLista.get(j).getStrMeasure5();
						String m6 = miaLista.get(j).getStrMeasure6();
						String m7 = miaLista.get(j).getStrMeasure7();
						String m8 = miaLista.get(j).getStrMeasure8();
						String m9 = miaLista.get(j).getStrMeasure9();
						String m10 = miaLista.get(j).getStrMeasure10();
						String m11 = miaLista.get(j).getStrMeasure11();
						String m12 = miaLista.get(j).getStrMeasure12();
						String m13 = miaLista.get(j).getStrMeasure13();
						String m14 = miaLista.get(j).getStrMeasure14();
						String m15 = miaLista.get(j).getStrMeasure15();
						
						Composizione co1 = new Composizione(drink,i1,m1);
						Composizione co2 = new Composizione(drink,i2,m2);
						Composizione co3 = new Composizione(drink,i3,m3);
						Composizione co4 = new Composizione(drink,i4,m4);
						Composizione co5 = new Composizione(drink,i5,m5);
						Composizione co6 = new Composizione(drink,i6,m6);
						Composizione co7 = new Composizione(drink,i7,m7);
						Composizione co8 = new Composizione(drink,i8,m8);
						Composizione co9 = new Composizione(drink,i9,m9);
						Composizione co10 = new Composizione(drink,i10,m10);
						Composizione co11 = new Composizione(drink,i11,m11);
						Composizione co12 = new Composizione(drink,i12,m12);
						Composizione co13 = new Composizione(drink,i13,m13);
						Composizione co14 = new Composizione(drink,i14,m14);
						Composizione co15 = new Composizione(drink,i15,m15);
						//dao in comp
						
						DaoFactory.getDaoFactory().ComposizioneDaoInterface().aggiungiComposizione(co1);
						DaoFactory.getDaoFactory().ComposizioneDaoInterface().aggiungiComposizione(co2);
						DaoFactory.getDaoFactory().ComposizioneDaoInterface().aggiungiComposizione(co3);
						DaoFactory.getDaoFactory().ComposizioneDaoInterface().aggiungiComposizione(co4);
						DaoFactory.getDaoFactory().ComposizioneDaoInterface().aggiungiComposizione(co5);
						DaoFactory.getDaoFactory().ComposizioneDaoInterface().aggiungiComposizione(co6);
						DaoFactory.getDaoFactory().ComposizioneDaoInterface().aggiungiComposizione(co7);
						DaoFactory.getDaoFactory().ComposizioneDaoInterface().aggiungiComposizione(co8);
						DaoFactory.getDaoFactory().ComposizioneDaoInterface().aggiungiComposizione(co9);
						DaoFactory.getDaoFactory().ComposizioneDaoInterface().aggiungiComposizione(co10);
						DaoFactory.getDaoFactory().ComposizioneDaoInterface().aggiungiComposizione(co11);
						DaoFactory.getDaoFactory().ComposizioneDaoInterface().aggiungiComposizione(co12);
						DaoFactory.getDaoFactory().ComposizioneDaoInterface().aggiungiComposizione(co13);
						DaoFactory.getDaoFactory().ComposizioneDaoInterface().aggiungiComposizione(co14);
						DaoFactory.getDaoFactory().ComposizioneDaoInterface().aggiungiComposizione(co15);
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			System.out.println("Sono al giro: "+(i+1));
		}		
	}
	
}
