package servlet;

import java.io.IOException;
import java.util.List;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import dao.DaoFactory;
import model.Drink;
/**
 * Servlet implementation class Foggienm
 */
@Path("cocktails")
@WebServlet("/cocktails")
public class Foggienm extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Foggienm() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    @GET
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
    	response.addHeader("Access-Control-Allow-Origin", "http://localhost:4200");
		String n="[";
		JsonArray nap=mandaCocktail();
		for (int i = 0; i < nap.size(); i++) {
			if (nap.size()-1!=i) {
				n+=nap.get(i).toString()+",";
			}else n+=nap.get(i).toString();
			
		}
		n+="]";
		response.getWriter().print(n);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	}
	

	public JsonArray mandaCocktail() {
        JsonArray ja = new JsonArray();
        List<Drink> drinks = DaoFactory.getDaoFactory().DrinkDaoInterface().getDrink();
        for (int i = 0; i < drinks.size(); i++) {
            ja.add(cocktail(drinks.get(i)));
        }
        return ja;
    }

    public JsonObject cocktail(Drink drink) {
        JsonObject jo = new JsonObject();
        jo.addProperty("nome", drink.getNome());
        jo.addProperty("bicchiere", drink.getBicchiere());
        jo.addProperty("alcolico", drink.getAlcoolNoAlcool());
        jo.addProperty("immagine", drink.getImmagine());
        jo.addProperty("categoria", drink.getCategoria().getNome());
        return jo;

    }

}
