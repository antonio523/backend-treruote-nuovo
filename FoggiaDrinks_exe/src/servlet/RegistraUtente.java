package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

import com.google.gson.Gson;

import model.Utente;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Servlet implementation class RegistraUtente
 */
@Path("registrazione")
@WebServlet("/registrazione")
public class RegistraUtente extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegistraUtente() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
    	response.addHeader("Access-Control-Allow-Origin", "*");
    	System.out.println("sono nel do get!");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
    @POST
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
    	response.addHeader("Access-Control-Allow-Origin", "*");
		System.out.println("sono nel do post!");
		/*response.addHeader("Access-Control-Allow-Origin", "http://localhost:4200");
		String path ="http://localhost:8080/FoggiaDrinks_exe/registrazione";
		Request req = new Request.Builder()
				.url(path)
				.build();
		String requestResult = null;
		try {
			Response res = new OkHttpClient().newCall(req).execute();
			if(res.code()==200) {
				Utente u = null;
				requestResult = res.body().string();
				System.out.println(requestResult);
				u = new Gson().fromJson(requestResult, Utente.class);
				System.out.println(u);
				
			}
		} catch (IOException e) {
			e.printStackTrace();
		}*/
	}

}
