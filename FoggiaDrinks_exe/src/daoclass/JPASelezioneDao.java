package daoclass;

import daointerface.SelezioneDaoInterface;

public class JPASelezioneDao implements SelezioneDaoInterface{

private final static JPASelezioneDao instance = new JPASelezioneDao();
	
	private JPASelezioneDao() {
		
	}
	public static JPASelezioneDao getInstance() {
		return instance;
	}
}