package daoclass;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import daointerface.UtenteDaoInterface;
import model.Utente;

public class JPAUtenteDao implements UtenteDaoInterface{

private final static JPAUtenteDao instance = new JPAUtenteDao();

    private JPAUtenteDao() {

    }
    public static JPAUtenteDao getInstance() {
        return instance;
    }


    public boolean aggiungiUtente(Utente u) {

        try {
            EntityManager manager =getManager();
            EntityTransaction transaction = manager.getTransaction();
            transaction.begin();
            manager.persist(u);
            transaction.commit();
        }catch(Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean rimuoviUtente (Utente u) {
        try{
            EntityManager manager= getManager();
            EntityTransaction transaction= manager.getTransaction();
            transaction.begin();
            Utente u2= manager.merge(u);
            manager.remove(u2);
            transaction.commit();
        }catch(Exception e){
            e.printStackTrace();
            return false;

        }
        return true;
    }

}