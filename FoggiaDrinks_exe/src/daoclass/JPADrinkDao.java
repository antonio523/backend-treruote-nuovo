package daoclass;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import daointerface.DrinkDaoInterface;
import model.Drink;
import model.Ingrediente;

public class JPADrinkDao implements DrinkDaoInterface{
	
private final static JPADrinkDao instance = new JPADrinkDao();
	
	private JPADrinkDao() {
		
	}
	public static JPADrinkDao getInstance() {
		return instance;
	}

	public boolean aggiungiDrink(Drink d) {

		try {
			EntityManager manager =getManager();
			EntityTransaction transaction = manager.getTransaction();
			transaction.begin();
			manager.persist(d);
			transaction.commit();
		}catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public void rimuoviDrink(Drink d) {
		try {
			EntityManager manager = getManager();
			EntityTransaction transaction = manager.getTransaction();
			transaction.begin();
			Drink d2 = manager.merge(d);
			manager.remove(d2);
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public List<Drink> getDrink(){
		try {
			EntityManager manager = getManager();
			List<Drink> drinks = (List<Drink>)manager.createNamedQuery("Drink.findAll", Drink.class).getResultList();
			return drinks;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public boolean updateDrink(Drink d) {
		EntityTransaction transaction;
		try {
			EntityManager manager = getManager();
			transaction = manager.getTransaction();
			transaction.begin();
			manager.merge(d);
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}
	
	public Drink getSingleDrinkByNome(String stringa) {
        try {
            EntityManager manager = getManager();
            Query query = manager.createQuery("SELECT c FROM Drink c WHERE c.nome=:x");
            query.setParameter("x", stringa);
            return (Drink) query.getSingleResult();
        }catch(NoResultException e) {
            return null;
        }catch(Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}