package daoclass;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import daointerface.IngredienteDaoInterface;
import model.Categoria;
import model.Ingrediente;

public class JPAIngredienteDao implements IngredienteDaoInterface {
	
	private final static JPAIngredienteDao instance = new JPAIngredienteDao();
			
			private JPAIngredienteDao() {}
			
			
			public static JPAIngredienteDao getInstance() {
				return instance;
			}


			public boolean aggiungiIngrediente(Ingrediente i) {

				try {
					EntityManager manager =getManager();
					EntityTransaction transaction = manager.getTransaction();
					transaction.begin();
					manager.persist(i);
					transaction.commit();
				}catch(Exception e) {
					e.printStackTrace();
					return false;
				}
				return true;
			}
			
			public void rimuoviIngrediente(Ingrediente i) {
				try {
					EntityManager manager = getManager();
					EntityTransaction transaction = manager.getTransaction();
					transaction.begin();
					Ingrediente i2 = manager.merge(i);
					manager.remove(i2);
					transaction.commit();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			public List<Ingrediente> getIngrediente(){
				try {
					EntityManager manager = getManager();
					List<Ingrediente> ingredienti = (List<Ingrediente>)manager.createNamedQuery("Ingrediente.findAll", Ingrediente.class).getResultList();
					return ingredienti;
				} catch (Exception e) {
					e.printStackTrace();
				}
				return null;
			}
			public Ingrediente getSingleIngredienteByNome(String stringa) {
		        try {
		            EntityManager manager = getManager();
		            Query query = manager.createQuery("SELECT c FROM Ingrediente c WHERE c.nome=:x");
		            query.setParameter("x", stringa);
		            return (Ingrediente) query.getSingleResult();
		        }catch(NoResultException e) {
		            return null;
		        }catch(Exception e) {
		            e.printStackTrace();
		        }
		        return null;
		    }
}

