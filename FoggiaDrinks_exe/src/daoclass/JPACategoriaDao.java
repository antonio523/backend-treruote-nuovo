package daoclass;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import daointerface.CategoriaDaoInterface;
import model.Categoria;


public class JPACategoriaDao implements CategoriaDaoInterface{

	private final static JPACategoriaDao instance = new JPACategoriaDao();
	
	private JPACategoriaDao() {
		
	}
	public static JPACategoriaDao getInstance() {
		return instance;
	}
	
	public boolean aggiungiCategoria(Categoria c) {

		try {
			EntityManager manager =getManager();
			EntityTransaction transaction = manager.getTransaction();
			transaction.begin();
			manager.persist(c);
			transaction.commit();
		}catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public void rimuoviCategoria(Categoria c) {
		try {
			EntityManager manager = getManager();
			EntityTransaction transaction = manager.getTransaction();
			transaction.begin();
			Categoria c2 = manager.merge(c);
			manager.remove(c2);
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public List<Categoria> getCategoria(){
		try {
			EntityManager manager = getManager();
			List<Categoria> categorie = (List<Categoria>)manager.createNamedQuery("Categoria.findAll", Categoria.class).getResultList();
			return categorie;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	public Categoria getSingleCategoriaByNome(String stringa) {
        try {
            EntityManager manager = getManager();
            Query query = manager.createQuery("SELECT c FROM Categoria c WHERE c.nome=:x");
            query.setParameter("x", stringa);
            return (Categoria) query.getSingleResult();
        }catch(NoResultException e) {
            return null;
        }catch(Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}

