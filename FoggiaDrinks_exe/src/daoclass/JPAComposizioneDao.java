package daoclass;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import daointerface.ComposizioneDaoInterface;
import model.Composizione;


public class JPAComposizioneDao implements ComposizioneDaoInterface{

	private final static JPAComposizioneDao instance = new JPAComposizioneDao();
			
		private JPAComposizioneDao() {}
			
		public static JPAComposizioneDao getInstance() {
			return instance;
		}
	
		public boolean aggiungiComposizione(Composizione c) {

			try {
				EntityManager manager=getManager();
				EntityTransaction transaction = manager.getTransaction();
				transaction.begin();
				manager.persist(c);
				transaction.commit();
			}catch(Exception e) {
				e.printStackTrace();
				return false;
			}
			return true;
		}
		
		public void rimuoviComposizione(Composizione c) {
			try {
				EntityManager manager = getManager();
				EntityTransaction transaction = manager.getTransaction();
				transaction.begin();
				Composizione c2 = manager.merge(c);
				manager.remove(c2);
				transaction.commit();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		public List<Composizione> getComposizione(){
			try {
				EntityManager manager = getManager();
				List<Composizione> composizione = (List<Composizione>)manager.createNamedQuery("Composizione.findAll", Composizione.class).getResultList();
				return composizione;
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}
}